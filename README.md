The repository contains CycleDroid: a bike computer application for Android. The application is available for free in Google Play:

[https://play.google.com/store/apps/details?id=com.maral.cycledroid](https://play.google.com/store/apps/details?id=com.maral.cycledroid)

I have ceased development of CycleDroid and made the code of the latest available version (1.9.9) open-source in order to allow anybody to contribute and continue development. The application is licensed under [GNU General Public License](https://gnu.org/licenses/gpl.html).

The repository contains:

* source code (Java, XML) of the application,
* all necessary libraries,
* resources: icons, images, etc.,
* editable versions of resources (PSD files),
* Eclipse configuration of the project,
* miscellaneous resources and scripts.

The repository lacks some private data: API key for Google Maps, keys required for Google In-app Billing, and Facebook application id.

You can easily import the application to Eclipse. Choose *File* > *New* > *Project...* > *Android* > *Android Project from Existing Code* and provide the main directory of this repository as *Root Directory*.