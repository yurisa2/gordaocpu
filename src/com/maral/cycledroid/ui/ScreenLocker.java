/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.ui;

import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.view.WindowManager;

import com.maral.cycledroid.WeakObserver;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.service.ServiceState.State;
import com.maral.cycledroid.service.TripService;

class ScreenLocker extends ActivityManager implements Observer {
	private final Settings settings;
	private boolean lock;
	
	public ScreenLocker(Activity activity, Settings settings) {
		super(activity);
		this.settings = settings;
		settings.addObserver(new WeakObserver(this));
		TripService.NOTIFIER.addObserver(new WeakObserver(this));
		lockChanged();
	}
	
	private void lockChanged() {
		boolean serviceDisabled = TripService.getState().getState() == State.SERVICE_DISABLED;
		lock = serviceDisabled || settings.getLockScreen();
		performAction();
	}
	
	public void update(Observable observable, Object data) {
		// doesn't check what type of setting has changed
		lockChanged();
	}
	
	@Override
	protected void performAction(Activity activity) {
		if(lock)
			activity.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		else
			activity.getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
	}
}
