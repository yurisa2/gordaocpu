/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.ui;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;

import com.maral.cycledroid.activity.settings.Settings;

public class ActivityManagerSettings extends ActivityManager {
	private final List<ActivityManager> subManagers = new ArrayList<ActivityManager>();
	
	public ActivityManagerSettings(Activity activity, Settings settings) {
		super(activity);
		subManagers.add(new OrientationChanger(activity, settings));
		subManagers.add(new ScreenLocker(activity, settings));
	}
	
	@Override
	public void attachActivity(Activity activity) {
		super.attachActivity(activity);
		for(ActivityManager sub : subManagers)
			sub.attachActivity(activity);
	}
	
	@Override
	public void detachActivity(Activity activity) {
		super.detachActivity(activity);
		for(ActivityManager sub : subManagers)
			sub.detachActivity(activity);
	}
	
	@Override
	protected void performAction(Activity activity) {
		for(ActivityManager sub : subManagers)
			sub.performAction(activity);
	}
}
