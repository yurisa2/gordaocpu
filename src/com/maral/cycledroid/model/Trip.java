/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.model;

import java.util.Observable;

import android.location.Location;

import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Angle;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;

public abstract class Trip extends Observable {
	public static final Altitude UNIT_ALTITUDE = Altitude.M;
	public static final Angle UNIT_ANGLE = Angle.DEGREE;
	public static final Distance UNIT_DISTANCE = Distance.M;
	public static final Duration UNIT_DURATION = Duration.MS;
	public static final Pace UNIT_PACE = Pace.S_PER_KM;
	public static final Speed UNIT_SPEED = Speed.MS;
	
	public static final long PART_TIME = 3000;
	
	public abstract Long getId();
	
	public abstract String getName();
	
	public abstract String getDescription();
	
	public abstract boolean providesEdit(); // edit
	
	public abstract void edit(String name, String description);
	
	public abstract boolean providesTracking(); // isPaused, addPoint, increaseTotalTime, pause, stop
	
	public abstract void addPoint(Location point);
	
	public abstract void increaseTotalTime(float deltaTime);
	
	public abstract void pause(); // pause for a while, clears just current speed and bearing
	
	public abstract boolean isPaused();
	
	public abstract void stop(); // stop definitely, clear all other volatile parameters
	
	public abstract float getDistance();
	
	public abstract float getTime();
	
	public abstract Float getCurrentSpeed();
	
	public abstract Float getMaxSpeed();
	
	public abstract Float getAltitude();
	
	private Float calculateAverageSpeed(float time) {
		if(Math.abs(time) < 0.1)
			return null;
		// sometimes reported average is higher than maximal, so do some trick
		float averageSpeed = getDistance() / (time / 1000.0f);
		Float maxSpeed = getMaxSpeed();
		return maxSpeed != null && averageSpeed >= maxSpeed? maxSpeed * 0.95f : averageSpeed;
	}
	
	public Float getAverageSpeed() {
		return calculateAverageSpeed(getTime());
	}
	
	public Float getPaceNetto() {
		float distance = getDistance();
		return distance < 1.0f? null : getTime() / distance;
	}
	
	public Float getPaceBrutto() {
		float distance = getDistance();
		return distance < 1.0f? null : getTotalTime() / distance;
	}
	
	public Float getSpeedBrutto() {
		return calculateAverageSpeed(getTotalTime());
	}
	
	public abstract float getElevationAsc();
	
	public abstract float getElevationDesc();
	
	public abstract Float getMinAltitude();
	
	public abstract Float getMaxAltitude();
	
	public abstract float getTotalTime();
	
	public abstract Float getBearing();
	
	public abstract Float getSlope();
	
	public abstract Float getPowerFactor();
	
	public abstract Float getInitialAltitude();
	
	public abstract Float getFinalAltitude();
	
	public abstract Long getStartTime();
	
	public abstract Long getEndTime();
	
	public Float getTimeFromStart() {
		Long startTime = getStartTime();
		Long endTime = getEndTime();
		return startTime == null || endTime == null? null : (float)(endTime - startTime);
	}
	
	public abstract boolean providesGraphs();
	
	public abstract boolean providesMap();
	
	public abstract boolean providesExport();
	
	public abstract boolean providesShare();
}
