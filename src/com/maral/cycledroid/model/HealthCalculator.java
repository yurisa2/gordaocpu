/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.model;

import java.util.Date;
import java.util.Observable;

import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.model.Unit.Energy;
import com.maral.cycledroid.model.Unit.Power;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

public class HealthCalculator extends Observable {
	public static final Energy UNIT_ENERGY = Energy.KCAL;
	public static final Power UNIT_POWER = Power.W;
	public static final Volume UNIT_VOLUME = Volume.L;
	public static final Weight UNIT_WEIGHT = Weight.G;
	
	private final Trip trip;
	private boolean male;
	private int birthYear;
	private Integer age = null; // cached
	private float weight;
	
	public HealthCalculator(Trip trip, boolean male, int birthYear, float weight) {
		this.trip = trip;
		this.male = male;
		this.birthYear = birthYear;
		this.weight = weight;
	}
	
	public Float getCaloriesBurned() {
		Float speed = Unit.convertSpeed(trip.getAverageSpeed(), Trip.UNIT_SPEED, Speed.MS);
		if(speed == null)
			return null;
		float timeMin = Unit.convertDuration(trip.getTime(), Trip.UNIT_DURATION, Duration.S) / 60.0f;
		float ascKcal = 9.80665f * Unit.convertAltitude(trip.getElevationAsc(), Trip.UNIT_ALTITUDE, Altitude.KM);
		float distKcal = (0.0048f * speed * speed - 0.04655f * speed + 0.2282f) * timeMin;
		float ageFactor = Math.max(1.06f - 0.0023f * getAge(), 0.7f);
		float sexFactor = male? 1.00f : 0.93f;
		return (ascKcal + distKcal) * ageFactor * sexFactor * weight;
	}
	
	public Float getFatBurned() {
		try {
			return getCaloriesBurned() / 7.71f;
		} catch(NullPointerException exception) {
			return null;
		}
	}
	
	public Float getOxygenConsumed() {
		try {
			return getCaloriesBurned() / 4.82f;
		} catch(NullPointerException exception) {
			return null;
		}
	}
	
	public Float getPower() {
		try {
			return trip.getPowerFactor() * weight;
		} catch(NullPointerException exception) {
			return null;
		}
	}
	
	private void performNotifyObservers() {
		setChanged();
		notifyObservers();
	}
	
	public void setMale(boolean male) {
		this.male = male;
		performNotifyObservers();
	}
	
	public void setBirthYear(int birthYear) {
		this.birthYear = birthYear;
		age = null; // force recomputation
		performNotifyObservers();
	}
	
	public void setWeight(float weight) {
		this.weight = weight;
		performNotifyObservers();
	}
	
	private int getAge() {
		if(age == null && trip.getStartTime() != null)
			age = Math.max(0, (new Date(trip.getStartTime())).getYear() + 1900 - birthYear); // max is just in case
		return age;
	}
}
