/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.model;

public class Unit {
	public static enum Altitude {
		M, KM, FT, MI, NMI,
	}
	
	public static enum Angle {
		DEGREE, PERCENT,
	}
	
	public static enum Distance {
		M, KM, FT, MI, NMI,
	}
	
	public static enum Duration {
		S, MS,
	}
	
	public static enum Energy {
		KCAL,
	}
	
	public static enum Pace {
		S_PER_KM, S_PER_MI, S_PER_NMI, MS_PER_M, MS_PER_FT,
	}
	
	public static enum Power {
		W, HP,
	}
	
	public static enum Speed {
		MS, KMH, FTS, MPH, KN,
	}
	
	public static enum Volume {
		L, GAL,
	}
	
	public static enum Weight {
		G, OZ,
	}
	
	private static float baseRate(Altitude unit) {
		switch(unit) {
			case M:
				return 1.0f;
			case KM:
				return 1000.0f;
			case FT:
				return 0.3048f;
			case MI:
				return 1609.344f;
			case NMI:
				return 1852.0f;
		}
		throw new IllegalArgumentException("Cannot convert altitude from/to unit " + unit + ".");
	}
	
	private static float baseRate(Angle unit) {
		switch(unit) {
			case PERCENT:
				return 1.0f;
			case DEGREE:
				return 100.0f / 90.0f;
		}
		throw new IllegalArgumentException("Cannot convert angle from/to unit " + unit + ".");
	}
	
	private static float baseRate(Distance unit) {
		switch(unit) {
			case M:
				return 1.0f;
			case KM:
				return 1000.0f;
			case FT:
				return 0.3048f;
			case MI:
				return 1609.344f;
			case NMI:
				return 1852.0f;
		}
		throw new IllegalArgumentException("Cannot convert distance from/to unit " + unit + ".");
	}
	
	private static float baseRate(Duration unit) {
		switch(unit) {
			case MS:
				return 1.0f;
			case S:
				return 1000.0f;
		}
		throw new IllegalArgumentException("Cannot convert duration from/to unit " + unit + ".");
	}
	
	private static float baseRate(Energy unit) {
		switch(unit) {
			case KCAL:
				return 1.0f;
		}
		throw new IllegalArgumentException("Cannot convert energy from/to unit " + unit + ".");
	}
	
	private static float baseRate(Pace unit) {
		switch(unit) {
			case S_PER_KM:
				return 1.0f;
			case S_PER_MI:
				return 1000.0f / baseRate(Distance.MI);
			case S_PER_NMI:
				return 1000.0f / baseRate(Distance.NMI);
			case MS_PER_FT:
				return 1.0f / baseRate(Distance.FT);
			case MS_PER_M:
				return 1.0f / baseRate(Distance.M);
		}
		throw new IllegalArgumentException("Cannot convert pace from/to unit " + unit + ".");
	}
	
	private static float baseRate(Power unit) {
		switch(unit) {
			case W:
				return 1.0f;
			case HP:
				return 735.49875f;
		}
		throw new IllegalArgumentException("Cannot convert power from/to unit " + unit + ".");
	}
	
	private static float baseRate(Speed unit) {
		switch(unit) {
			case MS:
				return 1.0f;
			case KMH:
				return 1.0f / 3.6f;
			case FTS:
				return 0.3048f;
			case MPH:
				return 1.609344f / 3.6f;
			case KN:
				return 0.514444f;
		}
		throw new IllegalArgumentException("Cannot convert speed from/to unit " + unit + ".");
	}
	
	private static float baseRate(Volume unit) {
		switch(unit) {
			case L:
				return 1.0f;
			case GAL:
				return 3.785411784f;
		}
		throw new IllegalArgumentException("Cannot convert volume from/to unit " + unit + ".");
	}
	
	private static float baseRate(Weight unit) {
		switch(unit) {
			case G:
				return 1.0f;
			case OZ:
				return 28.349523f;
		}
		throw new IllegalArgumentException("Cannot convert weight from/to unit " + unit + ".");
	}
	
	public static Float convertAltitude(Float value, Altitude from, Altitude to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertAngle(Float value, Angle from, Angle to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertDistance(Float value, Distance from, Distance to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertDuration(Float value, Duration from, Duration to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertEnergy(Float value, Energy from, Energy to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertPace(Float value, Pace from, Pace to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertSpeed(Float value, Speed from, Speed to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertPower(Float value, Power from, Power to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertVolume(Float value, Volume from, Volume to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
	
	public static Float convertWeight(Float value, Weight from, Weight to) {
		return value == null? null : value * baseRate(from) / baseRate(to);
	}
}
