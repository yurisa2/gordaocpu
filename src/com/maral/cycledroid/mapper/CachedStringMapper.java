/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.mapper;

import java.util.HashMap;
import java.util.Map;

import android.content.Context;

public class CachedStringMapper<T> implements StringMapper<T> {
	private final SimpleStringMapper<T> subMapper;
	private final Map<T, String> cache = new HashMap<T, String>();
	
	public CachedStringMapper(ResourceMapper<T> resources, Context context) {
		subMapper = new SimpleStringMapper<T>(resources, context);
	}
	
	public String getString(T object) {
		String fromCache = cache.get(object);
		return fromCache == null? subMapper.getString(object) : fromCache;
	}
}
