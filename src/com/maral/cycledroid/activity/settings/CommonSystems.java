/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.settings;

import java.lang.ref.WeakReference;

import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

final class CommonSystems {
	private CommonSystems() {} // prevent from creating
	
	public static final class ImperialSystem implements UnitsSystem {
		private static WeakReference<ImperialSystem> instance = new WeakReference<ImperialSystem>(null);
		
		private ImperialSystem() {}
		
		public static ImperialSystem getInstance() {
			ImperialSystem reference = instance.get();
			if(reference == null) {
				reference = new ImperialSystem();
				instance = new WeakReference<ImperialSystem>(reference);
			}
			return reference;
		}
		
		public Altitude getAltitudeUnit() {
			return Altitude.FT;
		}
		
		public Distance getDistanceUnit() {
			return Distance.MI;
		}
		
		public Pace getPaceUnit() {
			return Pace.S_PER_MI;
		}
		
		public Speed getSpeedUnit() {
			return Speed.MPH;
		}
		
		public Volume getVolumeUnit() {
			return Volume.GAL;
		}
		
		public Weight getWeightUnit() {
			return Weight.OZ;
		}
	}
	
	public static final class MetricSystem implements UnitsSystem {
		private static WeakReference<MetricSystem> instance = new WeakReference<MetricSystem>(null);
		
		private MetricSystem() {}
		
		public static MetricSystem getInstance() {
			MetricSystem reference = instance.get();
			if(reference == null) {
				reference = new MetricSystem();
				instance = new WeakReference<MetricSystem>(reference);
			}
			return reference;
		}
		
		public Altitude getAltitudeUnit() {
			return Altitude.M;
		}
		
		public Distance getDistanceUnit() {
			return Distance.KM;
		}
		
		public Pace getPaceUnit() {
			return Pace.S_PER_KM;
		}
		
		public Speed getSpeedUnit() {
			return Speed.KMH;
		}
		
		public Volume getVolumeUnit() {
			return Volume.L;
		}
		
		public Weight getWeightUnit() {
			return Weight.G;
		}
	}
}
