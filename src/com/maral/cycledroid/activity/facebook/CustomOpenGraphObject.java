/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import java.util.Date;
import java.util.List;
import java.util.Map;

import org.json.JSONObject;

import com.facebook.model.GraphObject;
import com.facebook.model.GraphObjectList;
import com.facebook.model.OpenGraphObject;

class CustomOpenGraphObject implements OpenGraphObject {
	private final OpenGraphObject subObject;
	
	public CustomOpenGraphObject(String type) {
		subObject = OpenGraphObject.Factory.createForPost(type);
	}
	
	public String getId() {
		return subObject.getId();
	}
	
	public void setId(String id) {
		subObject.setId(id);
	}
	
	public String getType() {
		return subObject.getType();
	}
	
	public void setType(String type) {
		subObject.setType(type);
	}
	
	public String getUrl() {
		return subObject.getUrl();
	}
	
	public void setUrl(String url) {
		subObject.setUrl(url);
	}
	
	public String getTitle() {
		return subObject.getTitle();
	}
	
	public void setTitle(String title) {
		subObject.setTitle(title);
	}
	
	public String getDescription() {
		return subObject.getDescription();
	}
	
	public void setDescription(String description) {
		subObject.setDescription(description);
	}
	
	public GraphObjectList<GraphObject> getImage() {
		return subObject.getImage();
	}
	
	public <T extends GraphObject> T cast(Class<T> graphObjectClass) {
		return subObject.cast(graphObjectClass);
	}
	
	public void setImage(GraphObjectList<GraphObject> images) {
		subObject.setImage(images);
	}
	
	public void setImageUrls(List<String> urls) {
		subObject.setImageUrls(urls);
	}
	
	public Map<String, Object> asMap() {
		return subObject.asMap();
	}
	
	public GraphObjectList<GraphObject> getVideo() {
		return subObject.getVideo();
	}
	
	public JSONObject getInnerJSONObject() {
		return subObject.getInnerJSONObject();
	}
	
	public void setVideo(GraphObjectList<GraphObject> videos) {
		subObject.setVideo(videos);
	}
	
	public Object getProperty(String propertyName) {
		return subObject.getProperty(propertyName);
	}
	
	public GraphObjectList<GraphObject> getAudio() {
		return subObject.getAudio();
	}
	
	public void setAudio(GraphObjectList<GraphObject> audios) {
		subObject.setAudio(audios);
	}
	
	public <T extends GraphObject> T getPropertyAs(String propertyName, Class<T> graphObjectClass) {
		return subObject.getPropertyAs(propertyName, graphObjectClass);
	}
	
	public String getDeterminer() {
		return subObject.getDeterminer();
	}
	
	public void setDeterminer(String determiner) {
		subObject.setDeterminer(determiner);
	}
	
	public <T extends GraphObject> GraphObjectList<T> getPropertyAsList(String propertyName, Class<T> graphObjectClass) {
		return subObject.getPropertyAsList(propertyName, graphObjectClass);
	}
	
	public List<String> getSeeAlso() {
		return subObject.getSeeAlso();
	}
	
	public void setSeeAlso(List<String> seeAlso) {
		subObject.setSeeAlso(seeAlso);
	}
	
	public String getSiteName() {
		return subObject.getSiteName();
	}
	
	public void setSiteName(String siteName) {
		subObject.setSiteName(siteName);
	}
	
	public void setProperty(String propertyName, Object propertyValue) {
		subObject.setProperty(propertyName, propertyValue);
	}
	
	public Date getCreatedTime() {
		return subObject.getCreatedTime();
	}
	
	public void removeProperty(String propertyName) {
		subObject.removeProperty(propertyName);
	}
	
	public void setCreatedTime(Date createdTime) {
		subObject.setCreatedTime(createdTime);
	}
	
	public Date getUpdatedTime() {
		return subObject.getUpdatedTime();
	}
	
	public void setUpdatedTime(Date updatedTime) {
		subObject.setUpdatedTime(updatedTime);
	}
	
	public GraphObject getApplication() {
		return subObject.getApplication();
	}
	
	public void setApplication(GraphObject application) {
		subObject.setApplication(application);
	}
	
	public boolean getIsScraped() {
		return subObject.getIsScraped();
	}
	
	public void setIsScraped(boolean isScraped) {
		subObject.setIsScraped(isScraped);
	}
	
	public String getPostActionId() {
		return subObject.getPostActionId();
	}
	
	public void setPostActionId(String postActionId) {
		subObject.setPostActionId(postActionId);
	}
	
	public GraphObject getData() {
		return subObject.getData();
	}
	
	public void setData(GraphObject data) {
		subObject.setData(data);
	}
	
	public boolean getCreateObject() {
		return subObject.getCreateObject();
	}
	
	public void setCreateObject(boolean createObject) {
		subObject.setCreateObject(createObject);
	}
}
