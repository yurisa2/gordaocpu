/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import java.util.ArrayList;
import java.util.List;

import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.MyCursor;
import com.maral.cycledroid.model.Trip;

class GetFacebookMetricsTask extends ExtendedAsyncTask {
	private static final int MAX_POINTS = 1000;
	
	private final Database database;
	private final Trip trip;
	private List<OpenGraphObjectMetric> metrics;
	
	public GetFacebookMetricsTask(AsyncTaskReceiver receiver, Database database, Trip trip) {
		super(receiver);
		this.database = database;
		this.trip = trip;
	}
	
	@Override
	protected void executeTask() {
		int pointsCount = database.getPointsCount(trip);
		if(pointsCount > 0) {
			int interval = 1 + pointsCount / MAX_POINTS;
			MyCursor cursor = null;
			boolean successful = false;
			database.beginTransaction();
			try {
				cursor = new MyCursor(database.getPointsCursor(trip, interval));
				metrics = new ArrayList<OpenGraphObjectMetric>();
				for(cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
					OpenGraphObjectMetric metric = new OpenGraphObjectMetric();
					metric.setLatitude(cursor.getFloat(Database.POINT_LATITUDE));
					metric.setLongitude(cursor.getFloat(Database.POINT_LONGITUDE));
					metric.setAltitude(cursor.getFloat(Database.POINT_ALTITUDE));
					metric.setTimestamp(Math.round(cursor.getFloat(Database.POINT_TIME)));
					metrics.add(metric);
				}
				successful = true;
			} finally {
				if(cursor != null)
					cursor.close();
				database.endTransaction(successful);
			}
		}
	}
	
	public List<OpenGraphObjectMetric> getMetrics() {
		return metrics;
	}
}
