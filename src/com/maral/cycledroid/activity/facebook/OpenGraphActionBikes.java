/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import java.util.Date;

class OpenGraphActionBikes extends CustomOpenGraphAction {
	public static final String TYPE = "fitness.bikes";
	public static final String PROPERTY_COURSE = "course";
	
	public OpenGraphActionBikes() {
		super();
		setType(TYPE);
	}
	
	public void setCourse(OpenGraphObjectCourse course) {
		setProperty(PROPERTY_COURSE, course);
	}
	
	public void setStartTime(long posixTime) {
		setStartTime(new Date(posixTime));
	}
	
	public void setEndTime(long posixTime) {
		setEndTime(new Date(posixTime));
	}
}
