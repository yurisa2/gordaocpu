/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.file;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import com.maral.cycledroid.R;

import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

class FilesListAdapter extends BaseAdapter {
	public static final String PATH_ROOT = "/";
	public static final String PATH_PARENT = "../";
	
	private final static Map<String, Integer> ICONS_MAP = new HashMap<String, Integer>();
	
	private final Context context;
	private final ListView listView;
	private final LayoutInflater inflater;
	private final String currentPath;
	private final List<File> filesList = new ArrayList<File>();
	private final boolean withCheckbox;
	private int filesCount = 0; // only files (not directories)
	
	static {
		String[] archive = {
			"7z", "ace", "bz2", "cab", "gz", "iso", "jar", "lz", "lzma", "rar", "tar", "tgz", "zip", "zipx",
		};
		assignExtensions(archive, R.drawable.file_icon_archive);
		
		String[] audio = {
			"aac", "ac3", "aif", "aiff", "flac", "mid", "midi", "mp1", "mp2", "mp3", "mpa", "mpga", "mpu", "ra", "snd",
			"wav", "wave", "wma",
		};
		assignExtensions(audio, R.drawable.file_icon_audio);
		
		String[] executable = {
			"dll", "exe", "o", "so",
		};
		assignExtensions(executable, R.drawable.file_icon_executable);
		
		String[] csv = {
			"csv",
		};
		assignExtensions(csv, R.drawable.file_icon_csv);
		
		String[] xml = {
			"gpx", "kml", "xml",
		};
		assignExtensions(xml, R.drawable.file_icon_xml);
		
		String[] document = {
			"abw", "doc", "docx", "dot", "dotx", "odm", "odt", "ott", "rtf", "wpd", "wps", "wpt", "wrd", "wrf", "wri",
		};
		assignExtensions(document, R.drawable.file_icon_document);
		
		String[] webpage = {
			"css", "htm", "html", "ihtml", "mht", "mhtml", "xht", "xhtml",
		};
		assignExtensions(webpage, R.drawable.file_icon_webpage);
		
		String[] pdf = {
			"dvi", "pdf", "ps",
		};
		assignExtensions(pdf, R.drawable.file_icon_pdf);
		
		String[] image = {
			"bmp", "cdr", "eps", "gif", "ico", "jpeg", "jpg", "mng", "odg", "png", "psd", "psp", "raw", "svg", "tif",
			"tiff", "wmf",
		};
		assignExtensions(image, R.drawable.file_icon_image);
		
		String[] presentation = {
			"odp", "otp", "pot", "pps", "ppt", "pptx", "sdd", "shf", "sti", "sxi",
		};
		assignExtensions(presentation, R.drawable.file_icon_presentation);
		
		String[] text = {
			"log", "txt", "readme",
		};
		assignExtensions(text, R.drawable.file_icon_text);
		
		String[] video = {
			"3gp", "asf", "avi", "flv", "m1v", "m2v", "fla", "m4v", "mkv", "mov", "mp4", "mpe", "mpeg", "mpg", "ogg",
			"rm", "swf", "wmv",
		};
		assignExtensions(video, R.drawable.file_icon_video);
		
		String[] spreadsheet = {
			"clf", "ods", "ots", "sdc", "stc", "sxc", "wks", "xlk", "xls", "xlsb", "xlsm", "xlsx", "xlr", "xlt",
			"xltm", "xlw",
		};
		assignExtensions(spreadsheet, R.drawable.file_icon_spreadsheet);
		
		String[] database = {
			"accdb", "accdc", "accde", "accdr", "accdt", "accdu", "accdw", "accft", "db", "mdb", "mde", "mdn", "mdt",
			"odb", "sdb", "sql", "sqlite", "sqlite3", "sqlitedb",
		};
		assignExtensions(database, R.drawable.file_icon_database);
	}
	
	private static void assignExtensions(String[] extensions, int icon) {
		for(int i = 0; i < extensions.length; ++i)
			ICONS_MAP.put(extensions[i], icon);
	}
	
	public FilesListAdapter(Context context, ListView listView, String currentPath, List<String> filesList,
			boolean withCheckbox) {
		this.context = context;
		this.listView = listView;
		this.inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		this.currentPath = currentPath;
		for(String filename : filesList) {
			File file = filename.compareTo(PATH_ROOT) == 0? new File(filename) : new File(currentPath + "/" + filename);
			if(!file.isHidden() || filename.compareTo(PATH_PARENT) == 0) { // completely omit hidden files
				this.filesList.add(file);
				if(file.isFile())
					++filesCount;
			}
		}
		this.withCheckbox = withCheckbox;
	}
	
	public int getFilesCount() {
		return filesCount;
	}
	
	public int getCount() {
		return filesList.size();
	}
	
	public File getItem(int position) {
		return filesList.get(position);
	}
	
	public long getItemId(int position) {
		return (long)getItem(position).hashCode();
	}
	
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView == null)
			convertView = inflater.inflate(R.layout.file_dialog_row, null);
		String filename = getItem(position).getName();
		if(filename.compareTo("") == 0) // for root the name is empty
			filename = PATH_ROOT;
		((TextView)convertView.findViewById(R.id.file_row_text)).setText(filename);
		((ImageView)convertView.findViewById(R.id.file_row_image)).setImageResource(getIcon(filename));
		CheckBox checkBox = (CheckBox)convertView.findViewById(R.id.file_checkbox);
		checkBox.setVisibility(withCheckbox? View.VISIBLE : View.GONE);
		checkBox.setEnabled(getItem(position).isFile());
		return convertView;
	}
	
	private int getIcon(String filename) {
		if(filename.compareTo(PATH_ROOT) == 0)
			return R.drawable.file_icon_directory;
		File file = new File(currentPath + "/" + filename);
		if(file.isDirectory())
			return R.drawable.file_icon_directory;
		String[] parts = filename.split("\\.");
		if(parts.length > 1) {
			Locale locale = context.getResources().getConfiguration().locale;
			Integer result = ICONS_MAP.get(parts[parts.length - 1].toLowerCase(locale));
			if(result != null)
				return result;
		}
		return R.drawable.file_icon_unknown;
	}
	
	private void checkCheckbox() {
		if(!withCheckbox)
			throw new IllegalStateException("This adapter was created without checkboxes.");
	}
	
	public boolean allChecked() {
		checkCheckbox();
		SparseBooleanArray checked = listView.getCheckedItemPositions();
		int checkedCount = 0;
		for(int i = 0; i < filesList.size(); ++i)
			if(checked.get(i))
				++checkedCount;
		return checkedCount == filesCount;
	}
	
	public void checkAll(boolean select) {
		checkCheckbox();
		for(int i = 0; i < filesList.size(); ++i)
			if(filesList.get(i).isFile())
				listView.setItemChecked(i, select);
	}
	
	public List<String> getChecked() {
		checkCheckbox();
		SparseBooleanArray checked = listView.getCheckedItemPositions();
		List<String> list = new ArrayList<String>();
		for(int i = 0; i < filesList.size(); ++i)
			if(checked.get(i))
				list.add(filesList.get(i).getName());
		return list;
	}
}
