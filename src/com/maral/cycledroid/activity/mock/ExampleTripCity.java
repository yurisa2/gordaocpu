/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.mock;

import android.content.Context;

import com.maral.cycledroid.R;

public class ExampleTripCity extends ExampleTrip {
	public ExampleTripCity(Context context) {
		super(context, R.string.example_city_name, R.string.empty_string);
	}
	
	@Override
	public Long getId() {
		return -4L;
	}
	
	@Override
	public float getDistance() {
		return 34581.59f;
	}
	
	@Override
	public float getTime() {
		return 7890000f;
	}
	
	@Override
	public Float getCurrentSpeed() {
		return 4.20f;
	}
	
	@Override
	public Float getMaxSpeed() {
		return 7.41f;
	}
	
	@Override
	public Float getAltitude() {
		return 379.95f;
	}
	
	@Override
	public float getElevationAsc() {
		return 38.88f;
	}
	
	@Override
	public float getElevationDesc() {
		return 45.12f;
	}
	
	@Override
	public Float getMinAltitude() {
		return 372.44f;
	}
	
	@Override
	public Float getMaxAltitude() {
		return 388.98f;
	}
	
	@Override
	public float getTotalTime() {
		return 12010000f;
	}
	
	@Override
	public Float getBearing() {
		return 249.00f;
	}
	
	@Override
	public Float getSlope() {
		return 0.0f;
	}
	
	@Override
	public Float getPowerFactor() {
		return 5.11f;
	}
	
	@Override
	public Float getInitialAltitude() {
		return 380.05f;
	}
	
	@Override
	public Long getStartTime() {
		return dateFromString("24.04.2013 16:35:21");
	}
	
	@Override
	public Long getEndTime() {
		return dateFromString("24.04.2013 19:57:15");
	}
}
