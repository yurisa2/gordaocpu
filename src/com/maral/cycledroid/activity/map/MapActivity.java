/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.map;

import java.lang.reflect.Method;
import java.util.Observable;
import java.util.Observer;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver.OnGlobalLayoutListener;
import android.widget.Toast;

import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.service.ServiceState.State;
import com.maral.cycledroid.service.TripService;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public final class MapActivity extends FragmentActivity {
	private static enum DialogType {
		NO_SERVICES, LEGAL_INFO, MAP_TYPE,
	}
	
	private static enum SavedInstanceKey {
		DRAWN_BEFORE, TASK_ID,
	}
	
	private static enum MapType {
		NORMAL, SATELLITE, HYBRID, TERRAIN,
	}
	
	private static enum IntentKey {
		TRIP_ID,
	}
	
	private class Controller implements Observer, OnGlobalLayoutListener, AsyncTaskReceiver {
		public void updateProgress(ExtendedAsyncTask task, int progress) {}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				progressDialog = new ProgressDialog(MapActivity.this);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setCancelable(false);
				progressDialog.setMessage(getString(R.string.progress_loading_data));
				progressDialog.show();
			}
		}
		
		public void taskFinishes(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				removeProgressDialog();
				GetMapPointsTask pointsTask = (GetMapPointsTask)task;
				mapManager = pointsTask.getManager();
				drawRoute();
				trip.addObserver(this);
				TripService.NOTIFIER.addObserver(this);
			}
		}
		
		public void onGlobalLayout() {
			if(!layoutDone) {
				layoutDone = true;
				if(mapManager != null)
					drawRoute();
			}
		}
		
		public void update(Observable observable, Object data) {
			if(observable == TripService.NOTIFIER) {
				State newState = TripService.getState().getState();
				previousState = newState;
				// Since drawLastPoint() removes marker and adds it again, we want to do it only if needed.
				if(!newState.equals(previousState) && (newState == State.FIX || previousState == State.FIX))
					drawLastPoint();
			} else if(observable == trip && data != null) {
				Location point = (Location)data;
				boolean empty = mapManager.isEmpty();
				mapManager.addLocation(point);
				if(empty) // draw a whole route
					drawRoute();
				else { // update an already drawn route
					drawLastPoint();
					if(follow)
						animateToPoint(MapManager.toLatLng(point));
				}
			}
			
		}
	}
	
	private final Controller controller = new Controller();
	
	// id in database of a trip to use
	public static final String INTENT_TRIP_ID = IntentKey.TRIP_ID.name();
	
	private AppInfo appInfo;
	private Database database;
	private Settings settings;
	private ActivityManager activityManager;
	private AsyncTaskQueue asyncTaskQueue;
	private Trip trip;
	
	private long taskId = ExtendedAsyncTask.NO_ID;
	private boolean layoutDone = false;
	private int routePadding; // "constant", will be set in onCreate(Bundle)
	private boolean follow = false;
	private boolean rotate = true;
	private boolean drawnBefore = false;
	private State previousState;
	
	private MapManager mapManager = null;
	private GoogleMap map;
	private View mapView = null;
	private ProgressDialog progressDialog = null;
	private Marker endMarker = null;
	private String[] mapTypes; // "constant"
	
	private void reflectInvalidateOptionsMenu() {
		try {
			Class<?> classView = Class.forName("android.support.v4.app.FragmentActivity");
			Method method = classView.getMethod("invalidateOptionsMenu");
			method.invoke(this, (Object[])null);
		} catch(Exception exception) {}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.map);
		try {
			drawnBefore = savedInstanceState.getBoolean(SavedInstanceKey.DRAWN_BEFORE.name());
			taskId = savedInstanceState.getLong(SavedInstanceKey.TASK_ID.name(), ExtendedAsyncTask.NO_ID);
		} catch(NullPointerException exception) {}
		
		appInfo = new AppInfo(this);
		follow = appInfo.getFollow();
		rotate = appInfo.getMapRotate();
		
		settings = SettingsSystem.getInstance(this);
		database = DatabaseSQLite.getInstance(this, settings);
		activityManager = new ActivityManagerSettings(this, settings);
		
		long tripId = getIntent().getLongExtra(INTENT_TRIP_ID, 0);
		trip = database.getTripsList().getById(tripId);
		setTitle(getString(R.string.label_map, trip.getName()));
		
		routePadding = Math.round(getResources().getDimension(R.dimen.map_padding));
		previousState = TripService.getState().getState();
		
		mapTypes = new String[MapType.values().length];
		mapTypes[MapType.NORMAL.ordinal()] = getString(R.string.map_type_normal);
		mapTypes[MapType.SATELLITE.ordinal()] = getString(R.string.map_type_satellite);
		mapTypes[MapType.HYBRID.ordinal()] = getString(R.string.map_type_hybrid);
		mapTypes[MapType.TERRAIN.ordinal()] = getString(R.string.map_type_terrain);
		
		SupportMapFragment fragment = (SupportMapFragment)getSupportFragmentManager().findFragmentById(R.id.map);
		mapView = fragment.getView();
		// http://stackoverflow.com/questions/13692579/movecamera-with-cameraupdatefactory-newlatlngbounds-crashes
		if(mapView.getViewTreeObserver().isAlive())
			mapView.getViewTreeObserver().addOnGlobalLayoutListener(controller);
		map = fragment.getMap();
		if(map == null) {
			showDialog(DialogType.NO_SERVICES.ordinal());
			return;
		}
		map.clear();
		map.setMapType(appInfo.getLastMapType());
		if(!drawnBefore) { // move to last remembered position
			CameraPosition position = appInfo.getLastCameraPosition();
			CameraUpdate update = CameraUpdateFactory.newCameraPosition(position);
			map.moveCamera(update);
		}
		
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		asyncTaskQueue.attach(controller);
		if(!(asyncTaskQueue.getLast() instanceof GetMapPointsTask)) {
			PolylineOptions options = new PolylineOptions();
			options.color(getResources().getColor(R.color.map_path));
			options.width(getResources().getDimension(R.dimen.map_path));
			options.visible(true);
			ExtendedAsyncTask task = new GetMapPointsTask(asyncTaskQueue, database, trip, new MapManager(options));
			taskId = task.getId();
			asyncTaskQueue.addTask(task);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityManager.detachActivity(this);
		removeProgressDialog();
		database.finish(isFinishing());
		if(map != null) {
			trip.deleteObserver(controller);
			TripService.NOTIFIER.deleteObserver(controller);
			appInfo.setLastCameraPosition(map.getCameraPosition());
			appInfo.setFollow(follow);
			appInfo.setMapRotate(rotate);
			asyncTaskQueue.detach(controller);
			if(isFinishing())
				asyncTaskQueue.clearLast();
		}
	}
	
	private int mapTypeToInt(MapType mapType) {
		switch(mapType) {
			case NORMAL:
				return GoogleMap.MAP_TYPE_NORMAL;
			case SATELLITE:
				return GoogleMap.MAP_TYPE_SATELLITE;
			case HYBRID:
				return GoogleMap.MAP_TYPE_HYBRID;
			case TERRAIN:
				return GoogleMap.MAP_TYPE_TERRAIN;
		}
		return GoogleMap.MAP_TYPE_NORMAL;
	}
	
	private MapType intToMapType(int mapType) {
		switch(mapType) {
			case GoogleMap.MAP_TYPE_NORMAL:
				return MapType.NORMAL;
			case GoogleMap.MAP_TYPE_SATELLITE:
				return MapType.SATELLITE;
			case GoogleMap.MAP_TYPE_HYBRID:
				return MapType.HYBRID;
			case GoogleMap.MAP_TYPE_TERRAIN:
				return MapType.TERRAIN;
		}
		return MapType.NORMAL;
	}
	
	private void drawFirstPoint(LatLng point) {
		MarkerOptions markerOptions = new MarkerOptions();
		markerOptions.position(point);
		markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_VIOLET));
		markerOptions.title(getString(R.string.route_start));
		map.addMarker(markerOptions);
	}
	
	private void drawLastPoint() {
		LatLng latLng = mapManager.getLastPoint();
		if(endMarker == null) {
			MarkerOptions markerOptions = new MarkerOptions();
			markerOptions.position(latLng);
			endMarker = map.addMarker(markerOptions);
		} else
			endMarker.setPosition(latLng);
		if(TripService.getState().getState() == State.FIX) {
			int icon;
			if(follow && rotate && !Float.isNaN(mapManager.getBearing()))
				icon = R.drawable.map_current_direction;
			else
				icon = R.drawable.map_current;
			endMarker.setIcon(BitmapDescriptorFactory.fromResource(icon));
			endMarker.setTitle(getString(R.string.current_position));
			endMarker.setAnchor(0.5f, 0.5f);
		} else {
			endMarker.setIcon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
			endMarker.setTitle(getString(R.string.route_end));
			endMarker.setAnchor(0.5f, 1.0f);
		}
	}
	
	private void drawRoute() {
		if(!mapManager.isEmpty() && layoutDone) {
			drawFirstPoint(mapManager.getFirstPoint());
			mapManager.attachPolyline(map.addPolyline(mapManager.getOptions()));
			drawLastPoint();
			if(!drawnBefore) {
				showRoute(true);
				drawnBefore = true;
			}
		}
		reflectInvalidateOptionsMenu();
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case NO_SERVICES:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_no_play_services);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
			case LEGAL_INFO:
				builder.setIcon(android.R.drawable.ic_dialog_info);
				builder.setTitle(R.string.legal_info);
				builder.setMessage(GooglePlayServicesUtil.getOpenSourceSoftwareLicenseInfo(this));
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
			case MAP_TYPE:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int item) {
						MapType mapType;
						try {
							mapType = MapType.values()[item];
						} catch(IndexOutOfBoundsException exception) {
							mapType = MapType.NORMAL;
						}
						int intType = mapTypeToInt(mapType);
						map.setMapType(intType);
						appInfo.setLastMapType(intType);
						removeDialog(id);
					}
				};
				builder.setTitle(R.string.select_map_type);
				builder.setSingleChoiceItems(mapTypes, intToMapType(map.getMapType()).ordinal(), listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putBoolean(SavedInstanceKey.DRAWN_BEFORE.name(), drawnBefore);
		outState.putLong(SavedInstanceKey.TASK_ID.name(), taskId);
	}
	
	private void animateToPoint(LatLng point) {
		CameraPosition.Builder builder = CameraPosition.builder(map.getCameraPosition());
		if(follow && rotate && !Float.isNaN(mapManager.getBearing()))
			builder.bearing(mapManager.getBearing());
		CameraPosition cameraPosition = builder.target(point).build();
		map.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
	}
	
	private void showRoute(boolean animate) {
		LatLngBounds bounds = mapManager.getBounds();
		if(bounds != null) {
			CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngBounds(bounds, routePadding);
			if(animate)
				map.animateCamera(cameraUpdate);
			else
				map.moveCamera(cameraUpdate);
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_location, menu);
		inflater.inflate(R.menu.option_follow, menu);
		inflater.inflate(R.menu.option_map_rotate, menu);
		inflater.inflate(R.menu.option_show_route, menu);
		inflater.inflate(R.menu.option_map_type, menu);
		inflater.inflate(R.menu.option_legal_info, menu);
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		menu.findItem(R.id.follow).setCheckable(true);
		menu.findItem(R.id.map_rotate).setCheckable(true);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		
		// disable some items, if maps are not available (no Google Play Services)
		boolean mapDisabled = mapManager == null;
		setCompletelyDisabled(menu.findItem(R.id.location), mapDisabled);
		setCompletelyDisabled(menu.findItem(R.id.follow), mapDisabled);
		setCompletelyDisabled(menu.findItem(R.id.map_rotate), mapDisabled);
		setCompletelyDisabled(menu.findItem(R.id.show_route), mapDisabled);
		setCompletelyDisabled(menu.findItem(R.id.map_type), mapDisabled);
		setCompletelyDisabled(menu.findItem(R.id.legal_info), mapDisabled);
		
		// set checkmark for option "Follow"
		menu.findItem(R.id.follow).setChecked(follow);
		
		// set state of option "Rotate map"
		menu.findItem(R.id.map_rotate).setChecked(rotate);
		menu.findItem(R.id.map_rotate).setEnabled(follow);
		
		// disable "Show route" if there are no points yet
		menu.findItem(R.id.show_route).setEnabled(mapManager != null? mapManager.getBounds() != null : false);
		// Above is important because onOptionsItemSelected doesn't check if there are any points.
		
		return super.onPrepareOptionsMenu(menu);
	}
	
	private void setCompletelyDisabled(MenuItem menuItem, boolean disabled) {
		menuItem.setEnabled(!disabled);
		menuItem.setVisible(!disabled);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.location:
				switch(TripService.getState().getState()) {
					case SERVICE_DISABLED:
						Toast.makeText(this, R.string.map_service_disabled, Toast.LENGTH_SHORT).show();
						break;
					case FIX:
						if(mapManager.getLastPoint() != null) {
							animateToPoint(mapManager.getLastPoint());
							break; // be careful on this code! :)
						}
					case ACQUIRING:
					case GPS_DISABLED:
					case NO_FIX:
						Toast.makeText(this, R.string.map_no_location, Toast.LENGTH_SHORT).show();
						break;
				}
				return true;
			case R.id.follow:
				if(follow)
					follow = false;
				else {
					switch(TripService.getState().getState()) {
						case SERVICE_DISABLED:
							Toast.makeText(this, R.string.map_service_disabled, Toast.LENGTH_SHORT).show();
							break;
						case FIX:
							if(mapManager.getLastPoint() != null) {
								animateToPoint(mapManager.getLastPoint());
								break; // be careful on this code! :)
							}
						case ACQUIRING:
						case GPS_DISABLED:
						case NO_FIX:
							Toast.makeText(this, R.string.map_follow_no_location, Toast.LENGTH_SHORT).show();
							break;
					}
					follow = true;
				}
				reflectInvalidateOptionsMenu();
				return true;
			case R.id.map_rotate:
				rotate = !rotate;
				if(mapManager.getLastPoint() != null) {
					drawLastPoint();
					animateToPoint(mapManager.getLastPoint()); // will rotate map
				}
				reflectInvalidateOptionsMenu();
				return true;
			case R.id.show_route: // This option is inactive if there are no points.
				if(follow) {
					follow = false;
					Toast.makeText(this, R.string.following_disabled, Toast.LENGTH_SHORT).show();
				}
				showRoute(true);
				reflectInvalidateOptionsMenu();
				return true;
			case R.id.map_type:
				showDialog(DialogType.MAP_TYPE.ordinal());
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.legal_info:
				showDialog(DialogType.LEGAL_INFO.ordinal());
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private void removeProgressDialog() {
		if(progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
