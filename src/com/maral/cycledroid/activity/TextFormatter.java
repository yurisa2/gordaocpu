/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity;

import android.content.Context;
import android.text.Html;
import android.text.SpannedString;
import android.text.TextUtils;

public class TextFormatter {
	private TextFormatter() {}
	
	public static CharSequence getText(Context context, int id, Object... args) {
		for(int i = 0; i < args.length; ++i)
			args[i] = args[i] instanceof String? TextUtils.htmlEncode((String)args[i]) : args[i];
		String formattedHtml = String.format(Html.toHtml(new SpannedString(context.getText(id))), args);
		// Android adds <p dir="ltr">...</p>, so remove it
		formattedHtml = formattedHtml.replaceAll("\\<p.*?\\>", "").replaceAll("\\</p\\>", "");
		return Html.fromHtml(formattedHtml);
	}
}
