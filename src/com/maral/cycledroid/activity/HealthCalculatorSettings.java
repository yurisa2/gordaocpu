/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity;

import java.util.Observable;
import java.util.Observer;

import com.maral.cycledroid.WeakObserver;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.Settings.SettingType;
import com.maral.cycledroid.activity.settings.Settings.Sex;
import com.maral.cycledroid.model.HealthCalculator;
import com.maral.cycledroid.model.Trip;

public class HealthCalculatorSettings extends HealthCalculator implements Observer {
	private final Settings settings;
	
	public HealthCalculatorSettings(Trip trip, Settings settings) {
		super(trip, settings.getSex() == Sex.MALE, settings.getBirthYear(), settings.getWeight());
		this.settings = settings;
		settings.addObserver(new WeakObserver(this));
	}
	
	public void update(Observable observable, Object data) {
		if(observable == settings) {
			switch((SettingType)data) {
				case CALORIES_AGE:
				case CALORIES_BIRTH_YEAR:
					super.setBirthYear(settings.getBirthYear());
					break;
				case CALORIES_SEX:
					super.setMale(settings.getSex() == Sex.MALE);
					break;
				case CALORIES_WEIGHT:
					super.setWeight(settings.getWeight());
					break;
				default: // uninteresting setting
			}
		}
	}
	
	private void throwSettingsException() {
		throw new UnsupportedOperationException("This parameter is set automatically by settings.");
	}
	
	@Override
	public void setBirthYear(int birthYear) {
		throwSettingsException();
	}
	
	@Override
	public void setMale(boolean male) {
		throwSettingsException();
	}
	
	@Override
	public void setWeight(float weight) {
		throwSettingsException();
	}
}
