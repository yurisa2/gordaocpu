/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import com.jjoe64.graphview.LabelFormatter;
import com.maral.cycledroid.formatter.ValueFormatter;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.Unit;
import com.maral.cycledroid.model.Unit.Duration;

class TimeFormatter implements LabelFormatter {
	private final ValueFormatter format;
	private final Duration durationUnit;
	
	public TimeFormatter(ValueFormatter format, Duration durationUnit) {
		this.format = format;
		this.durationUnit = durationUnit;
	}
	
	public String formatLabel(float value) {
		return format.formatDuration(Unit.convertDuration(value, Trip.UNIT_DURATION, durationUnit), durationUnit);
	}
}
