/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import com.jjoe64.graphview.GraphViewData;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.model.Trip;

class SpeedDistanceGenerator extends GraphDataGenerator {
	public SpeedDistanceGenerator(Database database, Trip trip) {
		super(database, trip);
	}
	
	@Override
	public float getMinX() {
		return 0.0f;
	}
	
	@Override
	public float getMaxX() {
		return getTrip().getDistance();
	}
	
	@Override
	public float getMinY() {
		return 0.0f;
	}
	
	@Override
	public float getMaxY() {
		Float result = getTrip().getMaxSpeed();
		return result == null? 0.0f : result;
	}
	
	@Override
	protected GraphViewData generatePair(TripData tripData) {
		return new GraphViewData(tripData.getDistance(), tripData.getCurrentSpeed());
	}
}
