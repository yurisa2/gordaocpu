/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import com.maral.cycledroid.model.TripSingle;

class TripData extends TripSingle {
	private static final String EXCEPTION_MESSAGE = "This class is used only for generating data for graphs.";
	
	public TripData() {
		super(null, null);
	}
	
	@Override
	public Long getId() {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}
	
	@Override
	public String getName() {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}
	
	@Override
	public String getDescription() {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}
	
	@Override
	public boolean providesEdit() {
		return false;
	}
	
	@Override
	public void edit(String name, String description) {
		throw new UnsupportedOperationException(EXCEPTION_MESSAGE);
	}
	
	@Override
	public boolean providesTracking() {
		return true;
	}
	
	@Override
	public boolean providesGraphs() {
		return false;
	}
	
	@Override
	public boolean providesMap() {
		return false;
	}
	
	@Override
	public boolean providesExport() {
		return false;
	}
}
