/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.jjoe64.graphview.GraphViewData;
import com.maral.cycledroid.CircularBuffer;
import com.maral.cycledroid.LocationBuilder;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.MyCursor;
import com.maral.cycledroid.model.Trip;

abstract class GraphDataGenerator {
	private static final int MAX_GRAPH_DATA = 10000;
	private static final int SMOOTH_PART = 50; // we average 1 / SMOOTH_PART
	
	private final Database database;
	private final Trip trip;
	private TripData tripData;
	private ArrayList<GraphViewData> data = new ArrayList<GraphViewData>();
	private int futureCount;
	
	public GraphDataGenerator(Database database, Trip trip) {
		this.database = database;
		this.trip = trip;
	}
	
	protected Trip getTrip() {
		return trip;
	}
	
	protected void prepareToGenerate() {
		data.clear();
		futureCount = 0;
		tripData = new TripData();
	}
	
	public void generateData(GenerateDataTask task) {
		task.updateProgress(0);
		
		boolean successful = false;
		database.beginTransaction();
		int interval = (database.getPointsCount(trip) - 1) / MAX_GRAPH_DATA + 1;
		MyCursor pointsCursor = new MyCursor(database.getPointsCursor(trip, 1));
		try {
			futureCount = (pointsCursor.getCount() - 1) / interval + 1;
			CircularBuffer<Float> lastData = new CircularBuffer<Float>(Math.max(1, futureCount / SMOOTH_PART));
			float sum = 0.0f;
			data.clear();
			data.ensureCapacity(futureCount); // for better performance
			
			int onePercent = pointsCursor.getCount() / 100;
			if(onePercent == 0)
				onePercent = 1; // to avoid dividing by zero in a loop
			int processed = 0;
			
			for(pointsCursor.moveToFirst(); !pointsCursor.isAfterLast(); pointsCursor.moveToNext()) {
				tripData.addPoint(LocationBuilder.createLocation(pointsCursor));
				if(processed % interval == 0) {
					GraphViewData pair = generatePair(tripData);
					sum += pair.valueY;
					Float deleted = lastData.add(pair.valueY);
					if(deleted != null)
						sum -= deleted;
					data.add(new GraphViewData(pair.valueX, sum / lastData.size()));
				}
				// don't do it often - it really slows:
				if(++processed % onePercent == 0)
					task.updateProgress((int)(processed / onePercent));
			}
			successful = true;
		} finally {
			task.updateProgress(100);
			pointsCursor.close();
			database.endTransaction(successful);
		}
	}
	
	public List<GraphViewData> getData() {
		return Collections.unmodifiableList(data);
	}
	
	public int getFutureCount() {
		return futureCount;
	}
	
	public abstract float getMinX();
	
	public abstract float getMaxX();
	
	public abstract float getMinY();
	
	public abstract float getMaxY();
	
	protected abstract GraphViewData generatePair(TripData tripData);
}
