/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.asynctask;

import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;

import android.os.AsyncTask.Status;

public class AsyncTaskQueueImpl implements AsyncTaskQueue {
	private static AsyncTaskQueueImpl instance = null;
	
	private final Queue<ExtendedAsyncTask> queue = new LinkedList<ExtendedAsyncTask>();
	private ExtendedAsyncTask last = null;
	private final Stack<AsyncTaskReceiver> receivers = new Stack<AsyncTaskReceiver>();
	
	private AsyncTaskQueueImpl() {}
	
	public static AsyncTaskQueueImpl getInstance() {
		if(instance == null)
			instance = new AsyncTaskQueueImpl();
		return instance;
	}
	
	public void updateProgress(ExtendedAsyncTask task, int progress) {
		for(AsyncTaskReceiver r : receivers)
			r.updateProgress(task, progress);
	}
	
	public void taskStarts(ExtendedAsyncTask task) {
		for(AsyncTaskReceiver r : receivers)
			r.taskStarts(task);
	}
	
	public void taskFinishes(ExtendedAsyncTask task) {
		for(AsyncTaskReceiver r : receivers)
			r.taskFinishes(task);
		nextTask();
	}
	
	public void addTask(ExtendedAsyncTask task) {
		queue.add(task);
		nextTask();
	}
	
	private void nextTask() {
		if((last == null || last.getStatus() == Status.FINISHED) && !queue.isEmpty()) {
			last = queue.poll();
			last.execute();
		}
	}
	
	public void attach(AsyncTaskReceiver receiver) {
		receivers.push(receiver);
		if(last != null) {
			receiver.taskStarts(last);
			receiver.updateProgress(last, last.getProgress());
			if(last.getStatus() == Status.FINISHED)
				receiver.taskFinishes(last);
		}
	}
	
	public void detach(AsyncTaskReceiver receiver) {
		if(!receivers.remove(receiver))
			throw new IllegalStateException("Receiver has not been attached.");
	}
	
	public ExtendedAsyncTask getLast() {
		return last;
	}
	
	public void clearLast() {
		last = null;
	}
}
