/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.exporter;

import java.io.File;
import java.io.Reader;
import java.io.Writer;

import com.maral.cycledroid.model.Trip;

public interface Exporter {
	// returns true if exported
	public boolean exportTrip(Trip trip, Writer writer, File file, ExportTripTask task);
	
	// returns imported trip (may be null if error)
	public Trip importTrip(Reader reader, File file, int lines, ImportTripTask task);
}
