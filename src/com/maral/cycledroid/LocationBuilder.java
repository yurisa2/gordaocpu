/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid;

import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.MyCursor;

import android.location.Location;

public class LocationBuilder {
	private static final String PROVIDER = "gps";
	
	private LocationBuilder() {} // prevent from creating an instance
	
	public static Location createLocation(float latitude, float longitude, float altitude, float speed, long time) {
		Location result = new Location(PROVIDER);
		result.setLatitude(latitude);
		result.setLongitude(longitude);
		result.setTime(time);
		result.setAltitude(altitude);
		result.setSpeed(speed);
		return result;
	}
	
	public static Location createLocation(MyCursor pointsCursor) {
		float latitude = pointsCursor.getFloat(Database.POINT_LATITUDE);
		float longitude = pointsCursor.getFloat(Database.POINT_LONGITUDE);
		float altitude = pointsCursor.getFloat(Database.POINT_ALTITUDE);
		float speed = pointsCursor.getFloat(Database.POINT_SPEED);
		long time = pointsCursor.getLong(Database.POINT_TIME);
		return createLocation(latitude, longitude, altitude, speed, time);
	}
	
	public static Location createLocation(float latitude, float longitude) {
		Location result = new Location(PROVIDER);
		result.setLatitude(latitude);
		result.setLongitude(longitude);
		return result;
	}
}
