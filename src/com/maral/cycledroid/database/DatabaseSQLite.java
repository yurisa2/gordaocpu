/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.database;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;
import java.util.Observable;
import java.util.Observer;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.location.Location;
import android.os.Handler;
import android.os.Looper;

import com.maral.cycledroid.LocationBuilder;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.Settings.SettingType;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripSingle;
import com.maral.cycledroid.model.TripsList;

public class DatabaseSQLite implements Database, Observer {
	private static final int DATABASE_VERSION = 9;
	private static final String DATABASE_NAME = "BikeCounter.sqlitedb";
	
	private class OpenHelper extends SQLiteOpenHelper {
		/* 4 - no columns TRIP_MAX_SPEED, TRIP_DISTANCE etc.
		 * 5 - columns TRIP_MAX_SPEED, TRIP_DISTANCE etc., compatible with version 4
		 * 6 - columns TRIP_MAX_SPEED, TRIP_DISTANCE etc., incompatible with version 5
		 * 7 - second page - elevation, min/max altitude, total time, calories
		 * 8 - distance calculated from location, no changes in database, but needs to be recalculated
		 * 9 - initial and final altitude */
		
		// create table
		private final String CREATE_ID_COLUMN = ID_COLUMN + " INTEGER PRIMARY KEY AUTOINCREMENT";
		
		private final String TRIP_CREATE_TABLE = String.format("CREATE TABLE %1$s (%2$s, %3$s TEXT NOT NULL, "
				+ "%4$s TEXT NOT NULL, %5$s REAL NULL DEFAULT NULL, %6$s REAL NOT NULL DEFAULT 0.0, "
				+ "%7$s INTEGER NOT NULL DEFAULT 0, %8$s REAL NOT NULL DEFAULT 0.0, %9$s REAL NOT NULL DEFAULT 0.0, "
				+ "%10$s REAL NULL DEFAULT NULL, %11$s REAL NULL DEFAULT NULL, %12$s INTEGER NOT NULL DEFAULT 0, "
				+ "%13$s REAL NULL DEFAULT NULL, %14$s REAL NULL DEFAULT NULL, %15$s INTEGER NULL DEFAULT NULL, "
				+ "%16$s INTEGER NULL DEFAULT NULL)", TRIP_TABLE, CREATE_ID_COLUMN, TRIP_NAME, TRIP_DESCRIPTION,
				TRIP_MAX_SPEED, TRIP_DISTANCE, TRIP_TIME, TRIP_ELEVATION_ASC, TRIP_ELEVATION_DESC, TRIP_MIN_ALTITUDE,
				TRIP_MAX_ALTITUDE, TRIP_TOTAL_TIME, TRIP_INITIAL_ALTITUDE, TRIP_FINAL_ALTITUDE, TRIP_START_TIME,
				TRIP_END_TIME);
		
		private final String POINT_CREATE_TABLE = String.format("CREATE TABLE %1$s (%2$s, %3$s INTEGER NOT NULL, "
				+ "%4$s REAL NOT NULL, %5$s REAL_NOT_NULL, %6$s REAL NOT NULL, %7$s INTEGER NOT NULL, "
				+ "%8$s REAL NOT NULL)", POINT_TABLE, CREATE_ID_COLUMN, POINT_TRIP, POINT_LATITUDE, POINT_LONGITUDE,
				POINT_ALTITUDE, POINT_TIME, POINT_SPEED);
		
		private final String TRIP_ALTER_TABLE_6_7[] = new String[] {
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_ELEVATION_ASC + " REAL NOT NULL DEFAULT 0.0",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_ELEVATION_DESC + " REAL NOT NULL DEFAULT 0.0",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_MIN_ALTITUDE + " REAL NULL DEFAULT NULL",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_MAX_ALTITUDE + " REAL NULL DEFAULT NULL",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_TOTAL_TIME + " INTEGER NOT NULL DEFAULT 0",
		};
		
		private final String TRIP_ALTER_TABLE_8_9[] = new String[] {
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_INITIAL_ALTITUDE + " REAL NULL DEFAULT NULL",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_FINAL_ALTITUDE + " REAL NULL DEFAULT NULL",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_START_TIME + " INTEGER NULL DEFAULT NULL",
			"ALTER TABLE " + TRIP_TABLE + " ADD COLUMN " + TRIP_END_TIME + " INTEGER NULL DEFAULT NULL",
		};
		
		// create trigger
		private static final String TRIP_TRIGGER = TRIP_TABLE + "_trigger";
		
		private final String TRIP_CREATE_TRIGGER = String.format("CREATE TRIGGER %1$s AFTER DELETE ON %2$s "
				+ "FOR EACH ROW BEGIN DELETE FROM %3$s WHERE %4$s = " + "OLD.%5$s; END", TRIP_TRIGGER, TRIP_TABLE,
				POINT_TABLE, POINT_TRIP, ID_COLUMN);
		
		private OpenHelper(Context context) {
			super(context, DATABASE_NAME, null, DATABASE_VERSION);
		}
		
		@Override
		public void onCreate(SQLiteDatabase database) {
			database.execSQL(TRIP_CREATE_TABLE);
			database.execSQL(POINT_CREATE_TABLE);
			database.execSQL(TRIP_CREATE_TRIGGER);
		}
		
		@Override
		public void onUpgrade(SQLiteDatabase database, int oldVersion, int newVersion) {
			if(oldVersion == newVersion)
				return;
			
			// structure of trip changed (elevations, min and max altitude, total time)
			if(oldVersion <= 6 && newVersion >= 7) {
				database.beginTransaction();
				boolean successful = false;
				try {
					for(int i = 0; i < TRIP_ALTER_TABLE_6_7.length; ++i)
						database.execSQL(TRIP_ALTER_TABLE_6_7[i]);
					successful = true;
				} finally {
					if(successful)
						database.setTransactionSuccessful();
					database.endTransaction();
				}
			}
			
			// structure of trip changed (initial and final altitude, start and end time)
			if(oldVersion <= 8 && newVersion >= 9) {
				database.beginTransaction();
				boolean successful = false;
				try {
					for(int i = 0; i < TRIP_ALTER_TABLE_8_9.length; ++i)
						database.execSQL(TRIP_ALTER_TABLE_8_9[i]);
					successful = true;
				} finally {
					if(successful)
						database.setTransactionSuccessful();
					database.endTransaction();
				}
			}
			
			// recalculate everything
			if(oldVersion <= 6 && newVersion >= 7 || oldVersion <= 8 && newVersion >= 9) {
				database.beginTransaction();
				boolean successful = false;
				try {
					// getTripsList() wouldn't work without below
					DatabaseSQLite.this.database = database;
					
					int onePercent = 1; // just in case
					if(task != null) {
						task.updateProgress(0);
						onePercent = getTotalPointsCount() / 100;
						if(onePercent == 0)
							onePercent = 1;
					}
					
					TripsList tripsList = getTripsList();
					database.execSQL("DROP TRIGGER " + TRIP_TRIGGER);
					database.execSQL("DROP TABLE " + TRIP_TABLE);
					final String POINT_TEMP_TABLE = "Point_temp";
					database.execSQL("ALTER TABLE " + POINT_TABLE + " RENAME TO " + POINT_TEMP_TABLE);
					onCreate(database);
					
					// add points again from temporary table and remove it
					int done = 0;
					for(Trip trip : tripsList) {
						Trip newTrip = createTripNoAdd(trip.getName(), trip.getDescription());
						MyCursor points = new MyCursor(getPointsCursor(trip, 1, POINT_TEMP_TABLE));
						try {
							for(points.moveToFirst(); !points.isAfterLast(); points.moveToNext()) {
								Location location = LocationBuilder.createLocation(points);
								addPoint(location, newTrip);
								if(task != null && ++done % onePercent == 0)
									task.updateProgress(done / onePercent);
							}
						} finally {
							points.close();
						}
						database.execSQL("DELETE FROM " + POINT_TEMP_TABLE + " WHERE " + POINT_TRIP + " = "
								+ trip.getId());
					}
					database.execSQL("DROP TABLE " + POINT_TEMP_TABLE);
					DatabaseSQLite.this.tripsList = null; // force recalculate next time
					
					successful = true;
				} finally {
					if(task != null)
						task.updateProgress(100);
					if(successful)
						database.setTransactionSuccessful();
					database.endTransaction();
				}
			}
		}
	}
	
	private static int instances = 0;
	private static DatabaseSQLite instance = null;
	
	private static final Collator collator = getCollator();
	
	private static final Comparator<Trip> COMPARATOR_ALPH_ASC = new Comparator<Trip>() {
		public int compare(Trip lhs, Trip rhs) {
			return collator.compare(lhs.getName(), rhs.getName());
		}
	};
	
	private static final Comparator<Trip> COMPARATOR_ALPH_DESC = new Comparator<Trip>() {
		public int compare(Trip lhs, Trip rhs) {
			return collator.compare(rhs.getName(), lhs.getName());
		}
	};
	
	private static final Comparator<Trip> COMPARATOR_CHRON_ASC = new Comparator<Trip>() {
		public int compare(Trip lhs, Trip rhs) {
			if(lhs.getStartTime() == null) {
				if(rhs.getStartTime() == null)
					return lhs.getId().compareTo(rhs.getId());
				else
					return 1;
			} else {
				if(rhs.getStartTime() == null)
					return -1;
				else
					return lhs.getStartTime().compareTo(rhs.getStartTime());
			}
		}
	};
	
	private static final Comparator<Trip> COMPARATOR_CHRON_DESC = new Comparator<Trip>() {
		public int compare(Trip lhs, Trip rhs) {
			return -COMPARATOR_CHRON_ASC.compare(lhs, rhs);
		}
	};
	
	private final Handler handler = new Handler(Looper.getMainLooper());
	private final Settings settings;
	private TripsList tripsList = null;
	
	private final GetSQLiteInstanceTask task;
	private SQLiteDatabase database;
	private boolean transaction = false;
	
	private DatabaseSQLite(Context context, Settings settings, GetSQLiteInstanceTask task) {
		this.settings = settings;
		this.task = task;
		this.database = (new OpenHelper(context)).getWritableDatabase();
		settings.addObserver(this);
	}
	
	public static final DatabaseSQLite getInstance(Context context, Settings settings) {
		return getInstance(context, settings, null);
	}
	
	protected static final DatabaseSQLite getInstance(Context context, Settings settings, GetSQLiteInstanceTask task) {
		if(instance == null)
			instance = new DatabaseSQLite(context, settings, task);
		++instances;
		return instance;
	}
	
	private static Float getFloatOrNull(MyCursor cursor, String column) {
		if(cursor.isNull(column))
			return null;
		return cursor.getFloat(column);
	}
	
	private static Long getLongOrNull(MyCursor cursor, String column) {
		if(cursor.isNull(column))
			return null;
		return cursor.getLong(column);
	}
	
	public TripsList getTripsList() {
		if(tripsList == null) {
			tripsList = new TripsList();
			String[] columns = new String[] {
				ID_COLUMN, TRIP_NAME, TRIP_DESCRIPTION, TRIP_MAX_SPEED, TRIP_DISTANCE, TRIP_TIME, TRIP_ELEVATION_ASC,
				TRIP_ELEVATION_DESC, TRIP_MIN_ALTITUDE, TRIP_MAX_ALTITUDE, TRIP_TOTAL_TIME, TRIP_INITIAL_ALTITUDE,
				TRIP_FINAL_ALTITUDE, TRIP_START_TIME, TRIP_END_TIME
			};
			MyCursor tripsCursor = new MyCursor(database.query(TRIP_TABLE, columns, null, null, null, null, null));
			try {
				for(tripsCursor.moveToFirst(); !tripsCursor.isAfterLast(); tripsCursor.moveToNext()) {
					long id = tripsCursor.getLong(ID_COLUMN);
					String name = tripsCursor.getString(TRIP_NAME);
					String description = tripsCursor.getString(TRIP_DESCRIPTION);
					Float maxSpeed = getFloatOrNull(tripsCursor, TRIP_MAX_SPEED);
					float distance = tripsCursor.getFloat(TRIP_DISTANCE);
					float time = (float)tripsCursor.getLong(TRIP_TIME);
					float elevationAsc = tripsCursor.getFloat(TRIP_ELEVATION_ASC);
					float elevationDesc = tripsCursor.getFloat(TRIP_ELEVATION_DESC);
					Float minAltitude = getFloatOrNull(tripsCursor, TRIP_MIN_ALTITUDE);
					Float maxAltitude = getFloatOrNull(tripsCursor, TRIP_MAX_ALTITUDE);
					float totalTime = (float)tripsCursor.getLong(TRIP_TOTAL_TIME);
					Float initialAltitude = getFloatOrNull(tripsCursor, TRIP_INITIAL_ALTITUDE);
					Float finalAltitude = getFloatOrNull(tripsCursor, TRIP_FINAL_ALTITUDE);
					Long startTime = getLongOrNull(tripsCursor, TRIP_START_TIME);
					Long endTime = getLongOrNull(tripsCursor, TRIP_END_TIME);
					TripSingle trip = new TripSingle(id, name, description, maxSpeed, distance, time, elevationAsc,
							elevationDesc, minAltitude, maxAltitude, totalTime, initialAltitude, finalAltitude,
							startTime, endTime);
					tripsList.add(trip);
				}
			} finally {
				tripsCursor.close();
			}
			sortTrips();
			tripsList.notifyObservers();
		}
		return tripsList;
	}
	
	private static Collator getCollator() {
		Collator collator = Collator.getInstance();
		collator.setStrength(Collator.SECONDARY);
		return collator;
	}
	
	private void sortTrips() {
		Comparator<Trip> comparator = null;
		
		switch(settings.getTripsSorting()) {
			case ALPH_ASC:
				comparator = COMPARATOR_ALPH_ASC;
				break;
			case ALPH_DESC:
				comparator = COMPARATOR_ALPH_DESC;
				break;
			case CHRON_ASC:
				comparator = COMPARATOR_CHRON_ASC;
				break;
			case CHRON_DESC:
				comparator = COMPARATOR_CHRON_DESC;
				break;
		}
		
		Collections.sort(tripsList, comparator);
		tripsList.notifyObservers();
	}
	
	public void beginTransaction() {
		transaction = true;
		database.beginTransaction();
	}
	
	public void endTransaction(boolean successful) {
		if(successful) {
			database.setTransactionSuccessful();
			Runnable runnable = new Runnable() {
				public void run() {
					sortTrips();
				}
			};
			handler.post(runnable);
		}
		database.endTransaction();
		transaction = false;
	}
	
	public void deleteTrip(final Trip trip) {
		String whereClause = ID_COLUMN + " = ?";
		String[] whereArgs = new String[] {
			Long.toString(trip.getId())
		};
		database.delete(TRIP_TABLE, whereClause, whereArgs);
		Runnable runnable = new Runnable() {
			public void run() {
				tripsList.remove(trip);
				if(!transaction)
					tripsList.notifyObservers();
			}
		};
		handler.post(runnable);
	}
	
	public Cursor getPointsCursor(Trip trip, int interval) {
		return getPointsCursor(trip, interval, POINT_TABLE);
	}
	
	private Cursor getPointsCursor(Trip trip, int interval, String table) {
		String selection = POINT_TRIP + " = ? AND " + ID_COLUMN + " % ? = 0";
		String selectionArgs[] = new String[] {
			Long.toString(trip.getId()), Integer.toString(interval)
		};
		String columns[] = new String[] {
			POINT_LATITUDE, POINT_LONGITUDE, POINT_ALTITUDE, POINT_TIME, POINT_SPEED
		};
		String orderBy = POINT_TIME + " ASC";
		return database.query(table, columns, selection, selectionArgs, null, null, orderBy);
	}
	
	private static ContentValues getPointCV(Location point, Trip trip) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(POINT_TRIP, trip.getId());
		contentValues.put(POINT_LATITUDE, point.getLatitude());
		contentValues.put(POINT_LONGITUDE, point.getLongitude());
		contentValues.put(POINT_ALTITUDE, point.getAltitude());
		contentValues.put(POINT_TIME, point.getTime());
		contentValues.put(POINT_SPEED, point.getSpeed());
		return contentValues;
	}
	
	public Trip createTripAndAdd(String name, String description) {
		Trip trip = createTripNoAdd(name, description);
		addTrip(trip);
		return trip;
	}
	
	public Trip createTripNoAdd(String name, String description) {
		TripSingle trip = new TripSingle(name, description);
		ContentValues contentValues = getChangingValues(trip);
		contentValues.put(TRIP_NAME, trip.getName());
		contentValues.put(TRIP_DESCRIPTION, trip.getDescription());
		trip.setId(database.insert(TRIP_TABLE, null, contentValues));
		return trip;
	}
	
	public void addTrip(final Trip trip) {
		Runnable runnable = new Runnable() {
			public void run() {
				getTripsList().add(trip);
				sortTrips();
				if(!transaction)
					tripsList.notifyObservers();
			}
		};
		handler.post(runnable);
	}
	
	public void addPoint(Location point, Trip trip) {
		trip.addPoint(point);
		database.insert(POINT_TABLE, null, getPointCV(point, trip));
		updateTripValues(trip);
		if(!transaction)
			tripsList.notifyObservers();
	}
	
	private static ContentValues getChangingValues(Trip trip) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(TRIP_MAX_SPEED, trip.getMaxSpeed());
		contentValues.put(TRIP_DISTANCE, trip.getDistance());
		contentValues.put(TRIP_TIME, (long)trip.getTime());
		contentValues.put(TRIP_ELEVATION_ASC, trip.getElevationAsc());
		contentValues.put(TRIP_ELEVATION_DESC, trip.getElevationDesc());
		contentValues.put(TRIP_MIN_ALTITUDE, trip.getMinAltitude());
		contentValues.put(TRIP_MAX_ALTITUDE, trip.getMaxAltitude());
		contentValues.put(TRIP_TOTAL_TIME, (long)trip.getTotalTime());
		contentValues.put(TRIP_INITIAL_ALTITUDE, trip.getInitialAltitude());
		contentValues.put(TRIP_FINAL_ALTITUDE, trip.getFinalAltitude());
		contentValues.put(TRIP_START_TIME, trip.getStartTime());
		contentValues.put(TRIP_END_TIME, trip.getEndTime());
		return contentValues;
	}
	
	private void updateTripValues(Trip trip) {
		ContentValues contentValues = getChangingValues(trip);
		String whereClause = ID_COLUMN + " = ?";
		String[] whereArgs = new String[] {
			Long.toString(trip.getId())
		};
		database.update(TRIP_TABLE, contentValues, whereClause, whereArgs);
	}
	
	public void updateTrip(Trip trip, String name, String description) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(TRIP_NAME, name);
		contentValues.put(TRIP_DESCRIPTION, description);
		String whereClause = ID_COLUMN + " = ?";
		String[] whereArgs = new String[] {
			Long.toString(trip.getId())
		};
		database.update(TRIP_TABLE, contentValues, whereClause, whereArgs);
		trip.edit(name, description);
		sortTrips();
		tripsList.notifyObservers();
	}
	
	public void increaseTotalTime(Trip trip, float deltaTime) {
		ContentValues contentValues = new ContentValues();
		contentValues.put(TRIP_TOTAL_TIME, (long)(trip.getTotalTime() + deltaTime));
		String whereClause = ID_COLUMN + " = ?";
		String[] whereArgs = new String[] {
			Long.toString(trip.getId())
		};
		database.update(TRIP_TABLE, contentValues, whereClause, whereArgs);
		trip.increaseTotalTime(deltaTime);
	}
	
	public int getPointsCount(Trip trip) {
		String columns[] = new String[] {
			"COUNT(*)"
		};
		String selection = POINT_TRIP + " = ?";
		String selectionArgs[] = new String[] {
			Long.toString(trip.getId())
		};
		Cursor cursor = database.query(POINT_TABLE, columns, selection, selectionArgs, null, null, null);
		try {
			cursor.moveToFirst();
			int result = cursor.getInt(0);
			return result;
		} finally {
			cursor.close();
		}
	}
	
	protected int getTotalPointsCount() {
		String columns[] = new String[] {
			"COUNT(*)"
		};
		Cursor cursor = database.query(POINT_TABLE, columns, null, null, null, null, null);
		try {
			cursor.moveToFirst();
			int result = cursor.getInt(0);
			cursor.close();
			return result;
		} finally {
			cursor.close();
		}
	}
	
	public void finish(boolean canClose) {
		if(--instances == 0 && canClose && instance != null) {
			if(database.isOpen())
				database.close();
			settings.deleteObserver(this);
			instance = null;
		}
	}
	
	public void update(Observable observable, Object data) {
		if(observable == settings && data instanceof SettingType) {
			if(((SettingType)data) == SettingType.TRIPS_SORTING) {
				sortTrips();
				tripsList.notifyObservers();
			}
		}
	}
}
