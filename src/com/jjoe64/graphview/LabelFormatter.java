package com.jjoe64.graphview;

public interface LabelFormatter {
	public abstract String formatLabel(float value);
}
